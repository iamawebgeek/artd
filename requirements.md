# Tower Defence Game

## Name
ARTD (Awesome React Tower Defence)

## Information
For this project you need to build a game in tower defence genre.

## Game requirements
- The game area should be 600x600 - px;
- Each cell in the game should be 100x100 - px;
- Add one type of tower to the game, with 50 damage per hit, 1 hit per second, 10 coins cost and hit range all the cells around it (including diagonally located cells too);
- Add one type of an enemy unit (MOB) of size 20x20 - px, with the speed of 1 cell in 2 seconds, hit points (health) 100 and 2 coins bonus for destroying;
- If MOB's hit points reach 0, then it disappears from the game area;
- Allow player to place a tower to any available cell, if current round has not started;
- Add the first level to the game, where you should put in the middle of the game area 2 straight horizontal paths for MOBs, so they can move from the left to the right side (in total 200x600 - px), all other cells should be enabled for the player to allocate towers, add 40 coins for a player on the start, release 10 MOBs in 3 seconds each of them randomly going either the first or the second line;
- Add "Go" button, which should start releasing current level mobs;
- Add "Restart" button, which should remove all towers, MOBs, messages and show the first round area;
- If any MOB reach the end of allocated for them path show a simple message to the player about his/her loss;
- If no MOBs left inside the game area show a message about successful beat of current round;

## Tech requirements
- Fork current repo;
- Send pull request with your solution;
- Use `React.js` library to develop the game;
- Use `Redux` library to keep the game state;
- Don't use `jQuery`;